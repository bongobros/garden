#!/bin/bash
rm -f /home/ben/Desktop/*.txt
mysuffix=$1
myip="$(hostname -I)"
mydns="$(nmcli device show | grep DNS)"
uuid=$(sudo cat "/sys/class/dmi/id/product_uuid")
#----
case "$uuid" in
	( A8129180-4104-11E2-8516-10604B65A84C ) uflags="-p beatrice -t 8 -v 1";;
	( 1BFF5280-F65B-11E1-A452-24BE050554DA ) uflags="-p cedric -t 6 -v 1";;
	( 945CC100-23E0-11E3-B7B6-7446A0B2FF2E ) uflags="-p lambert -t 8 -v 1";;
	( 4C4C4544-0032-3810-8051-B7C04F4E5731 ) uflags="-p ace -t 8 -v 1";;
#----4
	( 44454C4C-3600-1054-8052-C7C04F305231 ) uflags="-p francis -t 6 --cpu-affinity=0x3F";;
	( 5BEDD001-E22A-6A43-99D2-0D0DDA99803F ) uflags="-p ned -t 24 -v 1";;
	( 4C4C4544-0048-5A10-8052-CAC04F4D3232 ) uflags="-p pearl -t 12 -v 1";;
	( 442FFACE-1AB7-11E5-BEBB-D8CB8A783921 ) uflags="-p shemp -t 8 -v 1";;
	( E8B546BE-88BC-68B6-BD7C-B694253EB6C0 ) uflags="-p violet -t 8 -v 1";;
#----5
  ( 4C4C4544-004C-4E10-8036-B9C04F593132 ) uflags="-p ward -t 8 -v 1";;
	( 4C4C4544-0036-3910-8035-B9C04F5A3132 ) uflags="-p xavier -t 8 -v 1";;
	( 4C4C4544-0037-3810-8037-C4C04F503232 ) uflags="-p yancey -t 8 -v 1";;
	( 4C4C4544-0052-4310-8053-CAC04F563132 ) uflags="-p zevulon -t 8 -v 1";;
	( 4C4C4544-0048-5310-8037-B7C04F543532 ) uflags="-p franco -t 8 -v 1";;
#----5
	( 9081D6FC-D6AB-11E1-B0B2-E82218273800 ) uflags="-p biff -t 8 -v 1";;
	( A55E6180-D6AB-11E1-954F-E84BE0683700 ) uflags="-p cliff -t 8 -v 1";;
	( D6D638D4-D6A9-11E1-B906-D4C5A8C83300 ) uflags="-p douglas -t 8 -v 1";;
	( E1952800-AA4C-11E0-0000-2C27D742C845 ) uflags="-p eric -t 8 -v 1";;
#----4
	( 4C4C4544-0048-5310-8037-B7C04F543532 ) uflags="-p franco -t 8 -v 1";;
	( C0367B72-A734-F147-B086-2BB9148F399F ) uflags="-p grendel -t 24 -v 1";;
	( 1DF0EC00-FCD8-11E2-BA30-7446A0998ABB ) uflags="-p horace -t 8 -v 1";;
	( 4C4C4544-005A-3910-8048-C8C04F4D3232 ) uflags="-p indira -t 8 -v 1";;
	( 5D9B9780-2E31-11E3-BA55-8851FB71E9DF ) uflags="-p jasmine -t 8 -v 1";;
	( 54BC8E80-6479-11E2-BFA2-10604B6FF916 ) uflags="-p kira -t 8 -v 1";;
	( C875C080-4E2A-11E3-9EA6-2C44FD2C1BA4 ) uflags="-p lester -t 8 -v 1";;
	( 00134242-6CB0-114A-BF5F-885688549DFF ) uflags="-p moira -t 24 -v 1";;
	( 4C4C4544-0054-3810-8036-C7C04F4C5331 ) uflags="-p norbert -t 8 -v 1";;
	( 4C4C4544-0043-4410-8047-C7C04F4A5131 ) uflags="-p orenthal -t 8 -v 1";;
	( 4C4C4544-0050-3810-8033-B9C04F355631 ) uflags="-p peppa -t 8 -v 1";;
	( E0F77800-517A-11E1-0000-082E5F23A0D5 ) uflags="-p quincy -t 8 -v 1";;
	( 4C4C4544-0039-5010-8050-B4C04F443432 ) uflags="-p sanjay -t 8 -v 1";;
	( 983CA9DD-4A5C-814A-A02A-2F54F980AC38 ) uflags="-p tobias -t 24 -v 1";;
	( 9099BC58-583F-11E1-A5DB-E031001D3300 ) uflags="-p upton -t 8 -v 1";;
	( B2A03019-611A-52A1-1981-A17E0459A1AE ) uflags="-p vance -t 8 -v 1";;
	( 3C204F80-0D88-11E2-BEE4-A0B3CCFD4D04 ) uflags="-p wilbur -t 8 -v 1";;
	( 4C4C4544-004D-3010-8037-B9C04F593132 ) uflags="-p xander -t 8 -v 1";;
	( 4C4C4544-004E-4D10-8034-B6C04F583532 ) uflags="-p yolanda -t 8 -v 1";;
	( 4C4C4544-0057-5110-8036-B2C04F345631 ) uflags="-p zack -t 8 -v 1";;
	( 4C4C4544-0031-4A10-8039-B3C04F345631 ) uflags="-p archibald -t 8 -v 1";;
	( b ) uflags="-p bradley -t 8 -v 1";;
	( 4C4C4544-0057-3110-8052-B5C04F5A3132 ) uflags="-p carrie -t 8 -v 1";;

	( *	) uflags="-p unassignedneo";;
esac
#----
myname=$(echo $uflags|sed -e 's/-p //' -e 's/ -t.*//')
echo $myname, $uuid , $uflags, $myip, $mydns, $myname.$mysuffix.txt
echo $uflags > "/home/ben/Desktop/myflags.txt" #pass to payload

echo $myip,$uuid,$uflags,$mydns 				>  "/home/ben/Desktop/$myname.$mysuffix.txt"
/sbin/ifconfig | grep -E 'ether|HWaddr'			>> "/home/ben/Desktop/$myname.$mysuffix.txt"
sudo lshw -short -C disk,memory 			 	>> "/home/ben/Desktop/$myname.$mysuffix.txt"
sudo lshw -C memory | grep width			 	>> "/home/ben/Desktop/$myname.$mysuffix.txt"
#sudo smartctl --all /dev/sda | grep Power_On_Hours >> "/home/ben/Desktop/$myname.$mysuffix.txt"
#sudo smartctl --all /dev/sdb | grep Power_On_Hours >> "/home/ben/Desktop/$myname.$mysuffix.txt"
head "/home/ben/Desktop/log.txt" 			 	>> "/home/ben/Desktop/$myname.$mysuffix.txt"
tail "/home/ben/Desktop/log.txt" 			 	>> "/home/ben/Desktop/$myname.$mysuffix.txt"
date 			 								>> "/home/ben/Desktop/$myname.$mysuffix.txt"

curl -s -k -F "FileToUpload=@/home/ben/Desktop/$myname.$mysuffix.txt" https://137.143.156.121/bprojup/upload.dbw | grep submitted
#----------------------
#killall xterm ; /home/ben/Desktop/startscript.sh &
