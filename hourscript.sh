#!/bin/bash
if nc -zw1 www.google.com 443 &>/dev/null ; then
	cd ~/Desktop/garden
	git pull
	sh ./sendsitlog.sh "hour_v7"
	sh ./payloadh.sh &
else
	nmcli network off
	nmcli network on
	cd ~/Desktop/garden
	git pull
	sh ./sendsitlog.sh "FLIP"
	sh ./payloadh.sh &
fi
